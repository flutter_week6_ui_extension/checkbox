import 'package:checkbox/checkbox_widget.dart';
import 'package:checkbox/checkboxtile_widget.dart';
import 'package:checkbox/dropdown_widget.dart';
import 'package:checkbox/radio_widget.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'ui extension',
    home: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Ui Extension'),
        ),
        drawer: Drawer(
          child: ListView(
            children: [
              const DrawerHeader(
                child: Text('UI Menu'),
                decoration: BoxDecoration(color: Colors.blue),
              ),
              ListTile(
                title: const Text('CheckBox'),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CheckBoxWidget()));
                },
              ),
              ListTile(
                title: const Text('CheckBoxTile'),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CheckBoxTileWidget()));
                },
              ),
              ListTile(
                title: const Text('DropDown'),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DropDownWidget()));
                },
              ),
              ListTile(
                title: const Text('Radio'),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => RadioWidget()));
                },
              ),
            ],
          ),
        ),
        body: ListView(
          children: [
            ListTile(
              title: const Text('CheckBox'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => CheckBoxWidget()));
              },
            ),
            ListTile(
              title: const Text('CheckBoxTile'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CheckBoxTileWidget()));
              },
            ),
            ListTile(
              title: const Text('DropDown'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => DropDownWidget()));
              },
            ),
            ListTile(
              title: const Text('Radio'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => RadioWidget()));
              },
            ),
          ],
        ));
  }
}
